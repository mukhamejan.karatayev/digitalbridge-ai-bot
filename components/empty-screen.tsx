import { UseChatHelpers } from 'ai/react'

import { Button } from '@/components/ui/button'
import { ExternalLink } from '@/components/external-link'
import { IconArrowRight } from '@/components/ui/icons'

const exampleMessages = [
  {
    heading: 'Что может вызывать частые головные боли?',
    message: `Что может вызывать частые головные боли?`
  },
  {
    heading: 'Какие начальные методы лечения при растяжениях?',
    message: `Какие начальные методы лечения при растяжениях?`
  },
  {
    heading: 'К какому специалисту мне обратиться при болях в суставах?',
    message: `К какому специалисту мне обратиться при болях в суставах?`
  },
  {
    heading: 'Можете рассказать о контроле за диабетом?',
    message: `Можете рассказать о контроле за диабетом?`
  },
  // {
  //   heading: 'Что будет интересного на Digital Bridge 2023?',
  //   message: `Что будет интересного на Digital Bridge 2023?`
  // },
  // {
  //   heading: 'Где можно следить за новостями о Digital Bridge?',
  //   message: `Где можно следить за новостями о Digital Bridge?`
  // },
]

export function EmptyScreen({ setInput }: Pick<UseChatHelpers, 'setInput'>) {
  return (
    <div className="mx-auto max-w-2xl px-4">
      <div className="rounded-lg border bg-background p-8">
        <h1 className="mb-2 text-lg font-semibold">
          Добро пожаловать в AI Doctor Chatbot
        </h1>
        <p className="mb-2 text-md leading-normal text-muted-foreground">
          Это умный чат-бот, который поможет вам найти ответы на ваши вопросы связанные с медициной и здравоохранением.
        </p>
        <p className="leading-normal text-muted-foreground text-md">
          {/* You can start a conversation here or try the following examples: */}
          Вы можете начать диалог или попробовать следующие примеры:
        </p>
        <div className="mt-4 flex flex-col items-start space-y-2">
          {exampleMessages.map((message, index) => (
            <Button
              key={index}
              variant="link"
              className="h-auto p-0 text-md"
              onClick={() => setInput(message.message)}
            >
              <IconArrowRight className="mr-2 text-muted-foreground" />
              {message.heading}
            </Button>
          ))}
        </div>
      </div>
    </div>
  )
}
