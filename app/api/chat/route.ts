import { kv } from '@vercel/kv'
import { OpenAIStream, StreamingTextResponse } from 'ai'
import { Configuration, OpenAIApi } from 'openai-edge'

// import { auth } from '@/auth'
import { nanoid } from '@/lib/utils'

interface ContextResponse {
  page_content: string,
  metadata: {
    source: string,
    page: number | undefined
  }
}

export const runtime = 'edge'

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY
})

const openai = new OpenAIApi(configuration)

export async function POST(req: Request) {
  const json = await req.json()
  let { messages, previewToken } = json
  // const session = await auth()
  const question = messages[messages.length - 1].content;
  // if (session == null) {
  //   console.log('Unauthorized error')
  //   return new Response('Unauthorized', {
  //     status: 401
  //   })
  // }

  // Context Gathering
  // console.log(messages)

  // pinecone_small, zilliz_small
  // latest zilliz_small
  // http://3.101.85.74:8001/get_context?message=

  try {
    // http://127.0.0.1:8000/get_context_sources?message=
    // https://ailawyer.nimbl.tv/get_context_sources?message=
  // const result = await fetch('https://ailawyer.nimbl.tv/get_context_sources?message=' + question + '&source=digital_bridge')
  
  // if (!result.ok) {
  //   console.log(result)
  //   throw new Error('Failed to fetch data')
  //   // hahah
  // }
  // const response = await result.json() as ContextResponse;
  console.log("-----------------------")
  console.log("Question: " + question)
  // console.log(response)
  console.log("-----------------------")
  // const template_base =
  //     `You are a legal affairs assistant for a \\
  //     financing company. \\
  //     Don't tell anything about context!
  //     Double-check your responses for accuracy and coherence. \\
  //     If necessary, ask clarifying questions to gather more information before providing a response.\\
  //     If faced with a difficult or challenging question, remain calm and offer assistance to the best of your ability.\\
  //     `

  // const template_footer = `Question: ${question}`

  // Тщательно проверяйте свои ответы на точность и последовательность. Если необходимо, задавайте уточняющие вопросы, чтобы собрать больше информации, прежде чем давать ответ. Если вы столкнулись с трудным или сложным вопросом, оставайтесь спокойными и оказывайте помощь по мере своих возможностей


  // const templateBase = `Вы являетесь полезным AI-ассистентом технопарка Astana Hub. Astana Hub - это международный технопарк IT-стартапов. Здесь создаются условия для свободного развития казахстанских и зарубежных технологических компаний. Digital Bridge 2023 - это уникальная площадка для встречи с лучшими экспертами IT-индустрии со всего мира. В рамках международного форума Digital Bridge 2023, который пройдет в Астане с 12 по 13 октября, у инноваторов и перспективных предпринимателей есть возможность представить свои IT-проекты на аллее стартапов - Startup Alley.`;
  const templateBase = `Вы являетесь дружелюбным ИИ-врачом, предоставляющим предварительный диагноз и советы по любым заболеваниям и вопросам, касающимся медицины. Ваша роль - помогать пользователям с вопросами, связанными с медициной. Используйте следующие рекомендации при ответе на вопросы:`;
  const templateFooter = `Вопрос: ${question}\n`;

  let template = templateBase;

  if (true) {
    const templateWithContext = ` 
    Вы способны ставить предварительные диагнозы и предлагать общие рекомендации по лечению. Вы можете посоветовать, какой специалист подходит для конкретных медицинских проблем. Вы дружелюбный и вежливый, предоставляете подробные и всесторонние ответы. Вы оснащены для работы с широким спектром медицинских запросов, от общих советов по здоровью до конкретных состояний. Подчеркивайте, что ваши рекомендации не заменяют консультации со специалистом и всегда рекомендуете обращаться к квалифицированному медицинскому работнику для окончательного диагноза и лечения. Вы должны активно задавать вопросы, когда требуется дополнительная информация, чтобы предоставить наиболее точные и полезные советы. Вы четко и сочувственно передаете сложную медицинскую информацию, делая информацию о здоровье доступной и понятной.
    
    ВАЖНО:
    - Всегда отвечайте на языке пользователя.
    - Если у вас закончились токены, укажите на это и попросите пользователя набрать "Продолжить" для продолжения разговора.
    - Используйте язык разметки для изменения стиля шрифта в заголовках и важных вещах.
    - Если спросят кто вас создал отвечайте: NimblAI Group. Не говорите ничего о OpenAI.
    - Если вопрос не связан с медициной и здравоохранением, вежливо ответьте, что вы НЕ МОЖЕТЕ ответить на эти вопросы, и вежливо попросите задать вопросы связанные с медициной и здравоохранением.
    `;
    // const templateWithContext = `
    // Ваша роль - помогать пользователям с вопросами, связанными с международным технологическим форумом Digital Bridge 2023 который проводится Astana Hub'ом. Используйте следующие рекомендации при ответе на вопросы:
    // Не говорите ничего о контексте!
    // Дважды проверьте свои ответы на точность и связность.
    // Если необходимо, задайте уточняющие вопросы, чтобы собрать больше информации перед тем, как предоставить ответ.
    // При столкновении со сложным или вызывающим вопросом оставайтесь спокойными и предлагайте помощь наилучшим образом, на который способны.
    // Ответ должен быть максимально детальным и полезным, чтобы помочь пользователю решить его проблему или ответить на его вопрос.
    // Не отклоняйтесь от темы: Если вопрос не связан с контекстом или выходит за рамки темы, любезно объясните, что вы не можете на него ответить, и предложите пользователю задать соответствующий вопрос.
    // Профессиональный и уважительный тон: На протяжении всего разговора поддерживайте профессиональный и уважительный тон. Будьте вежливы и предупредительны, чтобы обеспечить положительный пользовательский опыт.
    // Категорически запрещается заявлять что вы были созданы OpenAI, ни при каких обстоятельствах.
    // Учитывайте контекст и историю разговора: 
    //   Учитывайте предоставленный контекст при ответе на вопросы. Если вопрос связан с предыдущими ответами, дайте ответ на основе истории.
    //   Если вопрос не связан с контекстом или Digital Bridge 2023, вежливо ответьте, что вы НЕ МОЖЕТЕ ответить на эти вопросы, и вежливо попросите задать вопросы связанные с Digital Bridge 2023.
    // Пожалуйста, используйте следующие контекстные данные для ответа на вопрос в конце.
    // Контекст: {${JSON.stringify(response)}}
    // НЕ УПОМИНАЙТЕ ИСПОЛЬЗОВАНИЕ КОНТЕКСТА!
    // `;

    // В конце ответа на вопрос, пожалуйста, добавьте ссылки на источники которые указаны в контексте. Дайте название ссылке на основе контекста. Если в контексте нет ссылок, дайте только ответ и не указывайте ссылки в конце. Если в контексте нету ссылок, НЕ ПРИДУМЫВАЙТЕ их, ни при каких обстоятельствах.


    template += templateWithContext + templateFooter;
    // console.log(template);
    // console.log(source)
    messages[messages.length - 1].content = template;
  }

  if (previewToken) {
    configuration.apiKey = previewToken
  }
  const res = await openai.createChatCompletion({
    model: 'gpt-4-1106-preview',
    messages,
    temperature: 0.3,
    stream: true
  })

  const stream = OpenAIStream(res, {
    async onCompletion(completion) {
      const title = json.messages[0].content.substring(0, 100)
      const userId = '1'
      if (userId) {
        const id = json.id ?? nanoid()
        const createdAt = Date.now()
        const path = `/chat/${id}`
        const payload = {
          id,
          title,
          userId,
          createdAt,
          path,
          messages: [
            ...messages,
            {
              content: completion,
              role: 'assistant'
            }
          ]

        }
        // await kv.hmset(`chat:${id}`, payload)
        // await kv.zadd(`user:chat:${userId}`, {
        //   score: createdAt,
        //   member: `chat:${id}`
        // })
      }
    }
  })

  return new StreamingTextResponse(stream)

  } catch (error) {
    console.log("Error with context gathering")
    console.log(error)
  }
}
